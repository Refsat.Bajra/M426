# M426 Software mit agilen Methoden entwickeln

(2. Lehrjahr ab AP21, 3. Lehrjahr bis AP20)


[>**Moduldefinition**](https://www.modulbaukasten.ch/modul/d075c9da-716c-eb11-b0b1-000d3a830b2b)


[Modulentwicklung ZH](https://gitlab.com/modulentwicklungzh/cluster-api/m426/-/tree/master/)


[Was ist neu im Scrum-Guide 2020](https://www.theprojectgroup.com/blog/scrum-guide-2020/)


## 1.) Vorgehensmodelle

### 1.1 Allgemeines
- [Vorgehensmodelle in internationalen Projekte](https://www.gulp.ch/knowledge-base/rund-ums-projekt/standards-und-vorgehensmodelle-in-internationalen-projekten.html)
- [Vorgehensmodelle, deAcademic](http://deacademic.com/dic.nsf/dewiki/1475124)
- [Vorgehensmodelle, Enzyklopädie W'Inf]( http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell)
- [Vorgehensmodelle, Sarre 2016](http://www.davit.de/uploads/media/Sarre-Vorgehensmodelle-v004.pdf)
- [Vorgehensmodelle und standardisierte Vorgehensweisen](http://www.techsphere.de/pageID=pm03.htm)
- [Vorgehensmodelle zum Softwareentwicklungsprozess](http://www.torsten-horn.de/techdocs/sw-dev-process.htm)
- [Wann welches Vorgehensmodell Sinn macht](http://www.elektronikpraxis.vogel.de/themen/embeddedsoftwareengineering/management/articles/273975)
- [SPEM Standard](1_Vorgehensmodelle/VM_00_Vorgehensmodelle/SPEM-Standard.pptx)
- [SPEM Standard OMG Document Number: formal/2008-04-01](1_Vorgehensmodelle/VM_00_Vorgehensmodelle/SPEM-Standard-formal-08-04-01.pdf)

### 1.2 Aufgabenerledigungsstruktur IPERKA
- [IPERKA 2014](1_Vorgehensmodelle/VM_01_IPERKA/IPERKA_1_4_D2014.pdf)
- [IPERKA](1_Vorgehensmodelle/VM_01_IPERKA/iperka.pdf)

### 1.3 Phasenmodell "Wasserfall"
- [Wasserfallmodell](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/Wasserfallmodell)

### 1.4 Phasenmodell "V-Modell-XT"
- [V-Modell-XT, Enzykolpädie](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/V-Modell-XT)
- [V-Modell-XT, Blogbeitrag](http://blog.pentadoc-gruppe.com/2011/07/16/vorgehensmodelle-im-pm-teil-3)

### 1.5 Phasenmodell "HERMES"
- [HERMES – der Schweizer Standard für IKT-Projekte im Überblick](1_Vorgehensmodelle/VM_04_Phasenmodell_HERMES/HERMES.pdf)

### 1.6 Phasenmodell "RUP"
- [Rational Unified Process (RUP)](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/Rational-Unified-Process-%28RUP%29)
- [RUP_notizen Dokument.pdf](1_Vorgehensmodelle/VM_05_Phasenmodell_RUP/RUP_notizen%20Dokument.pdf), [.docx](1_Vorgehensmodelle/VM_05_Phasenmodell_RUP/RUP_notizen%20Dokument.docx)

### 1.7 Prototyping
- [Vorgehensmodell Prototyping](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/Prototyping)

### 1.8 Spiralmodell
- [Spiralmodell](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/Spiralmodell)

### 1.9 Agiles XP
- [Extreme Programming XP](https://de.wikipedia.org/wiki/Extreme_Programming)
- [XP.pdf](1_Vorgehensmodelle/VM_08_Agile_XP/xp.pdf)



## 2.) SCRUM als Vorgehensmodell
- [scrumguides.org]( http://www.scrumguides.org)
- [The Scrum Guide, de](2_Vorgehensmodell_Scrum/Scrum-Guide-DE.pdf)
- [Open Assessments](https://www.scrum.org/open-assessments)
- [SCRUM Schulung & Zertifizierung](https://www.mitsm.de/scrum-schulung-zertifizierung)
- [VM_10 (7.51min) Introduction to SCRUM in 7 Minutes](https://www.youtube.com/watch?v=9TycLR0TqFA)
- [VM_11 (5.51min) Agiles Projektmanagement mit SCRUM (Teil 1)](https://www.youtube.com/watch?v=7UMMq8WmRNw&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=46)
- [VM_12 (4.56min) Agiles Projektmanagement mit SCRUM (Teil 2)](https://www.youtube.com/watch?v=wq3GcgZGSas&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=47)
- [VM_21 (4.29min) Agiles oder Klassisches Projektmanagement (Teil 1)](https://www.youtube.com/watch?v=JsxOhZypTu8&index=42&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO)
- [VM_22 (3.18min) Agiles oder Klassisches Projektmanagement (Teil 2)](https://www.youtube.com/watch?v=JqBZZpbwqIw&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=43)
- [VM_23 (5.38min) Agiles oder Klassisches Projektmanagement (Teil 3)](https://www.youtube.com/watch?v=MQ4pSPkLmf0&list=PLdPuPEPdfsEbu55aREtpwjtPALYrekPVO&index=44)
- [VM_31_Agile Vorgehensmodelle - vom Wasserfallmodell](https://blogs.itemis.com/de/scrum-kompakt-agile-vorgehensmodelle)
- [VM_31_Agile Vorgehensmodelle - zum Extreme-Programming](https://blogs.itemis.com/de/scrum-kompakt-extreme-programming-xp)
- [VM_32_Agile Vorgehensmodelle](http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Vorgehensmodell/Agile-Vorgehensmodelle)
- [VM_33_Vorgehensmodelle in der Softwareentwicklung und SCRUM](https://www.itemis.com/de/agile/scrum/kompakt/grundlagen-des-projektmanagements/vorgehensmodelle-in-der-softwareentwicklung)
- [M426_einige_Links_zu_klassischem_u_agilem_PM.pdf](2_Vorgehensmodell_Scrum/M426_einige_Links_zu_klassischem_u_agilem_PM.pdf), [.docx](2_Vorgehensmodell_Scrum/M426_einige_Links_zu_klassischem_u_agilem_PM.docx)


## 3.) How to Scrum

### 3.1 Allgemeines
- [Das agile Manifesto](https://agilemanifesto.org/iso/de/manifesto.html)
- [Der Scrum-Lebenszyklus](https://www.openpm.info/display/openPM/Scrum+Lebenszyklus)

### 3.2 Rollen, Team
- [Der/die ScrumMaster](3_HowToScrum/Scrummaster.pptx)
- [Der/die ProductOwner](./3_HowToScrum/productowner.pptx)
- [Wie ideale Teams funktionieren - Gemeinsam ans Ziel, PodCast 22:18 min, D, 2020-11-25](https://media.neuland.br.de/file/1810761/c/feed/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.mp3) <br> ---> [didaktische Fragen](3_HowToScrum/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.txt)
  
### 3.3 Handreichungen für den Start
- [GOagile_Checkliste_fuer_Meetings_01.pdf](3_HowToScrum/GOagile_Checkliste_fuer_Meetings_01.pdf)
- [10_The_Product_Vision_Board.pdf](3_HowToScrum/10_The_Product_Vision_Board.pdf)
- [Age-of-Product-Scrum-Anti-Patterns-Guide.pdf](3_HowToScrum/Age-of-Product-Scrum-Anti-Patterns-Guide-v38-2020-03-11.pdf)
- [Definitions of Done.pdf](3_HowToScrum/Definitions%20of%20Done.pdf)
- [Remote_Agile_Guide.pdf](3_HowToScrum/Remote_Agile_Guide.pdf)

### 3.4 Sprint Review
- [MindMap KAE](3_HowToScrum/Review/M426_MindMap_Sprint%20Review.jpg)

### 3.5 Sprint Retrospektive
- [MindMap KAE](3_HowToScrum/Retrospektive/M426_MindMap_Sprint%20Retrospektive.jpg)
- [Feedback im Team](http://meisterbar.de/wp-content/uploads/2017/03/IMG_4966.jpg)
- [Retrospekive in Scrum](https://i.pinimg.com/originals/7d/26/29/7d26294dbc559e8c95bcda7ff8c8e302.jpg)
- [Retromat - Ablauf einer Retrospektive zufällig zusammenstellen](https://retromat.org/de/)
- [Retrotool.io](https://retrotool.io)
- Starfish Retrospektive [-1-](https://cdn-images-1.medium.com/max/1200/0*9_kEHaPdivT3uGbg.jpg), [-2-](https://agileverwaltungorg.files.wordpress.com/2016/09/starfish-retrospektive.jpg)
- [Cartoon](3_HowToScrum/Retrospektive/M426_projektcartoon_015_retrospektive1.jpg)
- [Poster](3_HowToScrum/Retrospektive/Scrum_SprintRetrospektive_Postkarte.jpg)


## 4.) Ausgewählte Themen
Hier einige Themen zur Vertiefung oder als Unterlagen für Vorträge. Die Liste und der Umfang der Themen ist nicht abschliessend. Weitere Vorschläge sind willkommen und werden laufend erweitert.

### 4.1 Teamarbeit
- [Wie Teams funktionieren - Gemeinsam ans Ziel (mp3)](https://media.neuland.br.de/file/1810761/c/feed/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.mp3) <br>PodCast 22:18 min, D, 2020-11-25 ---> [didaktische Fragen](4_Erweiterungen/Teamarbeit/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.txt)

### 4.2 Entwurfsmuster
- <https://de.wikipedia.org/wiki/Entwurfsmuster_(Buch)>
- <https://de.wikipedia.org/wiki/Entwurfsmuster>
- <http://www.oodesign.com>
- <http://www.enzyklopaedie-der-wirtschaftsinformatik.de/lexikon/is-management/Systementwicklung/Softwarearchitektur>
 
### 4.3 Kontinuierliche Integration
- <https://docs.gitlab.com/ee/ci/pipelines.html>
- <https://jenkins.io/>
- <https://github.com/marketplace/category/continuous-integration>

### 4.4 Kundenzufriedenheit
- <https://de.wikipedia.org/wiki/Kano-Modell>
- [Kano and Progress Tracking](4_Erweiterungen/Kundenzufriedenheit/Kano%20and%20Progress%20Tracking.pdf)
- [Kano](4_Erweiterungen/Kundenzufriedenheit/Kano.pdf)
- [PlanningGame](4_Erweiterungen/Kundenzufriedenheit/PlanningGame.pdf)

### 4.5 Code Konventionen
- <http://checkstyle.sourceforge.net>
- <http://checkstyle.sourceforge.net/cmdline.html>
- <https://eslint.org/docs/user-guide/getting-started>
- <https://github.com/StyleCop/StyleCop>

### 4.6 Agile in a Nutshell - Posters
- <https://dandypeople.com/blog/enabling-business-agility-in-a-nutshell-free-infographic-poster>
- [Posters](4_Erweiterungen/Agile%20in%20a%20Nutshell%20-%20Posters)


## 5.) Tools

- [Scrum-Tools in GitHub](https://github.com/marketplace/category/project-management)
- <https://github.com/hermes5/websolution/issues>
- <https://www.easyproject.com/de>
- <https://about.gitlab.com>
- <https://de.atlassian.com/software/jira>
- <https://plan.io/de/>
- <https://taiga.io>
- <https://www.targetprocess.com>
- <https://www.taskworld.com>
- <https://trello.com>
- <https://www.zoho.com/>
<br/>weitere ...




## 6.)



## 7.) Begleitprodukte
Damit die agilen Methoden, die in diesem Kurs trainiert werden können, sollte zumindest ein Projekt als sog. Begleitprodukt behandelt werden, sonst kannman ja die anstehenden Herausforderungen die in der Teamarbeit entstehen, gar nicht gesehen werden.
- Grundsätzlich sollen Teams zu 4-9 Personen **eigene Projekte** Weiterführen oder neu entwickeln. 
- Wenn kein Projekt gefunden werden kann, können auch [Beispiele der Schule](7_Begleitprodukte) angegangen werden.

## 8.) 



## 9.) Vorträge

- [10 Dinge für eine gute Präsentation](https://wb-web.de/material/medien/10-dinge-die-sie-bei-prasentationen-dringend-beachten-sollten.html)
- [Wie halte ich einen Vortrag](https://www.psychologie.hu-berlin.de/de/prof/perdev/faq_entw_pers/WieHalteIchEinenVortrag)


**[Vortragsbewertungraster.pdf](./9_Vortraege/Vortragsbewertungraster_DistanzOnline.pdf) 
    ([.docx](./9_Vortraege/Vortragsbewertungraster_DistanzOnline.docx))**

---- **Vortragsthemen** zur Auswahl ----

- [CleanCode & Refactoring](9_Vortraege/Vortrag_Anwendung%20von%20CleanCode%20und%20Refactoring.html) --> [Checkstyle](http://checkstyle.sourceforge.net)
- [Refactoring, Wiederverwendbarkeit](9_Vortraege/Vortrag_Wiederverwendbarkeit_von_Code.html)
- [DesignPatterns](9_Vortraege/Vortrag_DesignPatterns.html)
- [Entwicklungsumgebungen](9_Vortraege/Vortrag_Entwicklungsumgebungen.html)
- [ExtremeProgramming](9_Vortraege/Vortrag_ExtremeProgramming.html)
- [CI/CD](9_Vortraege/Vortrag_Installations-Automatisierung%20und%20Continuous%20Integration%20Tools.html)
- [Scrum-Skalierung](9_Vortraege/Vortrag_Scrum-Skalierung.html)
- [SM & PO in der Praxis](9_Vortraege/Vortrag_ScrumMaster%20&%20ProductOwner%20in%20der%20Praxis.html)
- [Scrumtools](9_Vortraege/Vortrag_Scrumtools.html)
- [Stakeholder im Projekt](9_Vortraege/Vortrag_Stakeholder_im_Projekt.html)
- [Testingtools & automatisches Testen](9_Vortraege/Vortrag_Testing-Tools,%20Automatisches%20Testen.html)
- [Versionsverwaltungstools, Git und die weiteren Funktionen](9_Vortraege/Vortrag_Versionsverwaltungstools%20Git%20(detaillierte%20Funktionsweise).html)
- [Versionsverwaltungstools, Git vs. Subversion](9_Vortraege/Vortrag_Versionsverwaltungstools%20Git%20Vs.%20Subversion.html)
- [Versionsverwaltungs-Clouds: Vergleich GitHub / GitLab / BitBucket](9_Vortraege/Vortrag_Versionsverwaltungstools%20GitHub,%20GitLab,%20BitBucket.html)
- [Wie können Scrum-Projekte zum Fixpreis angeboten werden?](https://www.openpm.info/display/openPM/Projekte+mit+Scrum+zum+Festpreis)