Zeitaufwand ca. 45-60 min

Auftrag:
1.) Kopieren Sie diesen Datei-Inhalt in eine Textverarbeitungs-Datei
2.) Hören Sie diesen Audio-Beitrag und geben Sie die Antworten direkt bei den Fragen.
3.) Geben Sie, wenn verlangt, die Arbeit der Lehrperson ab.

https://media.neuland.br.de/file/1810761/c/feed/wie-ideale-teams-funktionieren-gemeinsam-ans-ziel.mp3


Wie ideale Teams funktionieren - Gemeinsam ans Ziel
===================================================
02:00 Unterschied in der Führung von Scott und Amundsen?
04:15 Was bedeutet Interdependenz?
04:20 Woher kommt der Begriff TEAM?

05:20-14:50  Fünf Punkte für die Zusammenstellung einer Gruppe
  05:25 1.) Was ist der erste Punkt?
            05:35 Was ist besser als ein Gruppen-Brainstorming?
  06:25 2.) Optimale Teamgrösse?
  07:25 3.) Durchmischung/Diversität der Gruppe. Welche?
            08:35 Warum Durchmischung der Gruppe
            08:45 Was ist Homophilie?  
  09:25 4.) Welche Typen (welche 5 Eigenschaften?)
            10:15 Drei positive Merkmale?
            10:50 Zwei weitere Merkmale?
  11:35 5.) Formelle und informelle Rollen, was sind diese? bis 12:40
            13:20 Wer und/oder was bringt das Team zusammen?
            13:45 Was ist die partizipative Sicherheit?
14:50 Warum ist es wichtig, das gemeinsame Ziel klarzumachen?

15:05 Was ist "Social Loafing"?
16:10 Wie holt man am meisten Leistung aus einer Gruppe?
16:40 Wie setzt man die Teammitglieder am Besten ein?
17:10 Warum Meetings? Nachteile 17:20? Vorteile 17:30?
18:05-18:50 Wie macht man effiziente Meetings?
18:55 Warum sind Meetings (nur) am Bildschirm problematisch?
19:50 Wir kann man machen, dass das Team besser zusammenarbeitet?
21:55 Was ist das menschliche Grundbedürfnis?